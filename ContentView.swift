import SwiftUI
import Combine

let relocate = PassthroughSubject<(Int,CGSize),Never>()
let disableDrag = PassthroughSubject<Bool,Never>()

struct ContentView: View {
    @State private var activeIdx: Int = 0
    @State private var rects: [CGRect] = Array<CGRect>(repeating: CGRect(), count: 36)
    @State private var cords:[CGSize] = Array<CGSize>(repeating: CGSize.zero, count: 36)
    @State private var link = 0
    @State private var joints = 4
    
    @State private var foods = 2
    @State private var foodx:[Double] = Array<Double>(repeating:Double(Int(Double.random(in: 16...UIScreen.main.bounds.width))) - 16.0, count: 8)
    @State private var foody:[Double] = Array<Double>(repeating: 0, count: 8)
    @State private var symbol:[Image] = Array<Image>(repeating: Image(systemName: "circle.fill"), count: 8)
    @State private var speed:[Double] = Array<Double>(repeating: 10, count: 8)
    @State private var deadly:[Double] = Array<Double>(repeating: UIScreen.main.bounds.height, count: 8)
    
    @State private var dragging = false
    @State private var ripe = 0
    @State private var missed = 0
    @State private var gameOver = false
    @State private var percent = ""
    
    let timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        ZStack(alignment: .center) {
            ForEach((0..<foods), id: \.self) { idx in
                symbol[idx]
                    .foregroundColor(returnColor(shape: symbol[idx]))
                    .frame(width: 16, height: 16, alignment: .center)
                    .position(x: foodx[idx], y: foody[idx])
                    .onReceive(disableDrag, perform: { value in
                        dragging = value
                    })
                    .onReceive(timer) { _ in
                        foody[idx] += speed[idx]
                        if foody[idx] > UIScreen.main.bounds.height {
                            foody[idx] = 0
                            foodx[idx] = Double.random(in: 16...UIScreen.main.bounds.width - 16)
                            if symbol[idx] == Image(systemName: "star.fill") {
                                joints -= 1
                                if !gameOver { missed += 1 }
                            }
                            symbol[idx] = Image(systemName: "circle.fill")
                            if foods < symbol.count { foods += 1 }
                        }
                        if rects[0].contains(CGPoint(x:foodx[idx],y:foody[idx])) && !dragging {
                            foody[idx] = 0
                            foodx[idx] = Double.random(in: 16...UIScreen.main.bounds.width - 16)
                            switch symbol[idx] {
                            case Image(systemName: "circle.fill"):
                                joints += 1
                                if !gameOver { ripe += 1 }
                            case Image(systemName: "diamond.fill"):
                                joints += 1
                            case Image(systemName: "square.fill"):
                                joints -= 1
                            case Image(systemName: "triangle.fill"):
                                joints -= 1
                            case Image(systemName: "star.fill"):
                                joints -= 1
                            default:
                                assert(false,"Never happens")
                            }
                            if !gameOver { missed += 1 }
                            
                            let rand = Int.random(in: 0...3)
                            let screenHeight = UIScreen.main.bounds.height
                            switch rand {
                            case 0:
                                symbol[idx] = Image(systemName: "circle.fill")
                                speed[idx] = Double.random(in: 4...8)
                            case 1:symbol[idx] = Image(systemName: "diamond.fill")
                                speed[idx] = Double.random(in: 8...12)
                            case 2:symbol[idx] = Image(systemName: "square.fill")
                                speed[idx] = Double.random(in: 4...8)
                            case 3:symbol[idx] = Image(systemName: "triangle.fill")
                                speed[idx] = Double.random(in: 8...12)
                            default:
                                assert(false,"never happens")
                            }
                            deadly[idx] = Double.random(in: screenHeight/4...screenHeight)
                        }
                        if foody[idx] > deadly[idx] {
                            if symbol[idx] != Image(systemName: "square.fill") &&
                                symbol[idx] != Image(systemName: "triangle.fill") {
                                symbol[idx] = Image(systemName: "star.fill")
                            }
                        }
                    }
            }
            if joints > 0 {
                ForEach((0..<joints - 1), id: \.self) { idx in
                    LineView(sCord: rects[idx], eCord: rects[idx + 1])
                }
                VStack {
                    VStack {
                        ForEach (0..<joints, id: \.self) { idx in
                            SnakeView(cords: $cords, dragging: $dragging, idx: idx)
                        }
                    }.onPreferenceChange(TextPreferenceKey.self) { preferences in
                        for p in preferences {
                            self.rects[p.viewIdx] = p.rect
                        }
                    }.animation(.easeInOut(duration: 1.0), value: rects)
                }
            } else {
                VStack {
                    Text("GAME OVER") { $0.kern = CGFloat(2) }
                    .font(Fonts.markerFeltThin(size: 48))
                    .bold()
                    .padding()
                    .onTapGesture {
                        joints = 4
                        foods = 1
                        gameOver = false
                    }.onAppear {
                        gameOver = true
                        percent = String(format: "%.1f", Double(ripe)/Double(missed) * 100)
                    }.onDisappear {
                        ripe = 0
                        missed = 0
                        foody = foody.map { $0 * 0 }
                    }
                    Text("Berries Caught \(ripe)") { $0.kern = CGFloat(2) }
                    .font(Fonts.markerFeltThin(size: 16))
                    .padding(.bottom, 1)
                    
                    Text("Berries Missed \(missed)") { $0.kern = CGFloat(2) }
                    .font(Fonts.markerFeltThin(size: 16))
                    .padding(.bottom, 6)
                    Text("\(percent) %")
                        .font(Fonts.markerFeltThin(size: 48))
                    
                }
            }
        }.coordinateSpace(name: "leZstack")
        
    }
    func returnColor(shape:Image) -> Color {
        switch shape {
        case Image(systemName: "circle.fill"):
            return Color.purple
        case Image(systemName: "diamond.fill"):
            return Color.blue
        case Image(systemName: "square.fill"):
            return Color.yellow
        case Image(systemName: "triangle.fill"):
            return Color.red
        default:
            // returns a star
            return Color.orange
        }
    }
}

struct LineView: View {
    let sCord: CGRect!
    let eCord: CGRect!
    @State var text: String = "?"
    @State var isShowing = false
    var body: some View {
        Path { path in
            path.move(to: CGPoint(x:sCord.midX, y:sCord.midY))
            path.addLine(to: CGPoint(x: eCord.midX, y: eCord.midY))
            path.closeSubpath()
        }.stroke(.orange, style: StrokeStyle(lineWidth: 20, lineCap: .round, lineJoin: .round))
    }
}

struct SnakeView: View {
    @Binding var cords:[CGSize]
    @Binding var dragging:Bool
    
    @GestureState var press = false
    @GestureState var drag:DragGesture!
    
    let idx: Int
    var body: some View {
        RoundedRectangle(cornerRadius: 8)
            .stroke(Color.black, lineWidth: 2)
            .frame(width: 24, height: 24)
            .background(PreferenceViewSetter(idx: idx))
            .overlay(Circle()
                        .fill(Color.white)
                        .overlay(Text(String(idx, radix: 2))
                        .font(Fonts.markerFeltThin(size: 12)))
                        .gesture(
                            DragGesture()
                                .onChanged { gesture in
                                    cords[idx].width = cords[idx].width + gesture.translation.width
                                    cords[idx].height = cords[idx].height + gesture.translation.height
                                    dragging = true
                                }
                                .onEnded({ gesture in
                                    relocate.send((idx + 1,cords[idx]))
                                    dragging = false
                                }))
                        .onTapGesture {
            })
            .offset(cords[idx])
            .onReceive(relocate) { value in
                let (id,newSize) = value
                if id == idx {
                    withAnimation(.linear(duration: 0.1 + Double(idx)/10)) {
                        cords[id] = newSize
                    }
                    Task { try! await Task.sleep(seconds: 0.1)
                        relocate.send((idx + 1,cords[idx]))
                    }
                }
            }
    }
}

struct PreferenceData: Equatable {
    let viewIdx: Int
    let rect: CGRect
}

struct TextPreferenceKey: PreferenceKey {
    typealias Value = [PreferenceData]
    
    static var defaultValue: [PreferenceData] = []
    
    static func reduce(value: inout [PreferenceData], nextValue: () -> [PreferenceData]) {
        value.append(contentsOf: nextValue())
    }
}

struct PreferenceViewSetter: View {
    let idx: Int
    
    var body: some View {
        GeometryReader { geometry in
            Rectangle()
                .fill(Color.clear)
                .preference(key: TextPreferenceKey.self,
                            value: [PreferenceData(viewIdx: self.idx, rect: geometry.frame(in: .named("leZstack")))])
        }
    }
}

extension Task where Success == Never, Failure == Never {
    static func sleep(seconds: Double) async throws {
        let duration = UInt64(seconds * 1000_000_000)
        try await sleep(nanoseconds: duration)
    }
}

extension Text {
    init(_ string: String, configure: ((inout AttributedString) -> Void)) {
        var attributedString = AttributedString(string)
        configure(&attributedString)
        self.init(attributedString)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}